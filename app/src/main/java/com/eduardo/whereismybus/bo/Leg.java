package com.eduardo.whereismybus.bo;

/**
 * Created by eduardo on 12/01/2017.
 */

public class Leg {
    public Duration distance;
    public Duration duration;
    public String end_address;
    public Location end_location;
    public String start_address;
    public Location start_location;
    public Step[] steps;
}
