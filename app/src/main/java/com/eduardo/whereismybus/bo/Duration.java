package com.eduardo.whereismybus.bo;

/**
 * Created by eduardo on 11/01/2017.
 */

public class Duration {
    private int value;
    private String text;

    public void setValue(int value){
        this.value = value;
    }

    public int getValue(){
        return value;
    }

    public void setText(String text){
        this.text = text;
    }

    public String getText(){
        return text;
    }
}
