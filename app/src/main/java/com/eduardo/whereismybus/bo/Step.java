package com.eduardo.whereismybus.bo;

/**
 * Created by eduardo on 11/01/2017.
 */

public class Step {
    public Duration distance;
    public Duration duration;
    public String travel_mode;
    public Location end_location;
    public String html_instructions;
    public Polyline polyline;
    public Location start_location;
}
