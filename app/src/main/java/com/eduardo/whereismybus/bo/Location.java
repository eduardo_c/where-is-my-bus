package com.eduardo.whereismybus.bo;

/**
 * Created by eduardo on 11/01/2017.
 */

public class Location {
    private double lat;
    private double lng;

    public void setLat(double lat){
        this.lat = lat;
    }

    public double getLat(){
        return lat;
    }

    public void setLng(double lng){
        this.lng = lng;
    }

    public double getLng(){
        return lng;
    }
}
