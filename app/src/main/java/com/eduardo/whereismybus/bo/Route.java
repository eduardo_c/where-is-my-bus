package com.eduardo.whereismybus.bo;

/**
 * Created by eduardo on 12/01/2017.
 */

public class Route {

    public Bounds bounds;
    public String copyrights;
    public Leg[] legs;
    public OverviewPolyline overview_polyline;
    public String summary;
    public int[] waypoint_order;
}
