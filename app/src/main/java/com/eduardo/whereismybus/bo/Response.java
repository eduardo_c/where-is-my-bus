package com.eduardo.whereismybus.bo;

/**
 * Created by eduardo on 12/01/2017.
 */

public class Response {
    public String status;
    public GeocodedWaypoint[] geocoded_waypoints;
    public Route[] routes;
}
