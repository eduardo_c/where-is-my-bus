package com.eduardo.whereismybus.controller;

import android.content.Context;

import com.eduardo.whereismybus.R;
import com.eduardo.whereismybus.bo.Response;
import com.eduardo.whereismybus.ws.IDirectionsApi;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by eduardo on 20/01/2017.
 */

public class DirectionsApiController {
    private final String BASE_URL = "https://maps.googleapis.com/maps/api/";
    private String mode = "walking";
    private String units = "metric";

    public Response getDirections(Context context, LatLng origin, LatLng destination){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        IDirectionsApi service = retrofit.create(IDirectionsApi.class);

        String originParsed = String.valueOf(origin.latitude) + "," + String.valueOf(origin.longitude);
        String destinationParsed = String.valueOf(destination.latitude) + "," + String.valueOf(destination.longitude);

        Call<Response> call = service.getDirections(originParsed,
                    destinationParsed,
                    context.getResources().getString(R.string.google_maps_directions_api_key),
                    mode,
                    units);
        try {
            return call.execute().body();
        }
        catch (IOException e){
            e.printStackTrace();
            return null;
        }
   }

}
