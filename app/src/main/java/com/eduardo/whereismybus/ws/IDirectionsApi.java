package com.eduardo.whereismybus.ws;

import retrofit2.Call;
import com.eduardo.whereismybus.bo.Response;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by eduardo on 18/01/2017.
 */

public interface IDirectionsApi {
    @GET("directions/json")
    Call<Response> getDirections(@Query("origin") String origin,
                                 @Query("destination") String destination,
                                 @Query("key") String key,
                                 @Query("mode") String mode,
                                 @Query("units") String units);
}
