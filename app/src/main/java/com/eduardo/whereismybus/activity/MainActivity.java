package com.eduardo.whereismybus.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.eduardo.whereismybus.R;
import com.eduardo.whereismybus.bo.OverviewPolyline;
import com.eduardo.whereismybus.bo.Response;
import com.eduardo.whereismybus.controller.DirectionsApiController;
import com.eduardo.whereismybus.util.GPSUtils;
import com.eduardo.whereismybus.util.PolylineParser;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Retrofit;

import static android.R.id.list;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, GoogleMap.OnMapLongClickListener {

    private final int ACCESS_FINE_LOCATION_PERMISSION_CODE = 0;
    private final int ACCESS_FINE_LOCATION_REMOVE_CODE = 1;
    private final int GET_LAST_KNOWN_LOCATION_CODE = 2;
    private final int VALIDATE_LOCATION_CODE = 3;

    private Location mLocation;
    private LocationManager mLocationManager;
    private GoogleMap mMap = null;
    private Marker userMarker;
    private Marker destinationMarker;
    private ProgressDialog progressDialog;
    private LocationListener mLocationListener = new LocationListener() {

        @Override
        public void onLocationChanged(Location location) {
            if(GPSUtils.isBetterLocation(location, mLocation)){
                mLocation = location;
                //Toast.makeText(MainActivity.this, "Precisión: " + location.getAccuracy(), Toast.LENGTH_SHORT).show();
                updateMarker(location.getLatitude(), location.getLongitude());
            }
            /*if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MainActivity.this.VALIDATE_LOCATION_CODE);
            } else {
                if(GPSUtils.isBetterLocation(location, mLocation)) {
                    mLocation = location;
                    updateMarker(location.getLatitude(), location.getLongitude());
                }
            }*/
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        progressDialog = new ProgressDialog(MainActivity.this);
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    this.ACCESS_FINE_LOCATION_PERMISSION_CODE);
        } else {
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mLocationListener);
            //mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, mLocationListener);
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GetGooglrDirectionsTask directionsTask = new GetGooglrDirectionsTask(MainActivity.this,
                        userMarker.getPosition(),
                        destinationMarker.getPosition());
                try{
                    progressDialog.setMessage("Obteniendo ruta");
                    progressDialog.show();
                    progressDialog.setCancelable(false);
                    progressDialog.setCanceledOnTouchOutside(false);
                    directionsTask.execute();
                }
                catch (Exception e){
                    e.printStackTrace();
                    Toast.makeText(MainActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapLongClickListener(MainActivity.this);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    this.GET_LAST_KNOWN_LOCATION_CODE);
        } else {
            Location location = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if(location != null){
                mLocation = location;
                Toast.makeText(this, "Precisión: " + location.getAccuracy(), Toast.LENGTH_SHORT).show();
                updateMarker(location.getLatitude(), location.getLongitude());
            }
        }
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(18);
        mMap.animateCamera(zoom);
    }

    private void updateMarker(double lat, double lon) {
        if(userMarker != null)
            userMarker.remove();
        if (mMap != null) {
            userMarker = mMap.addMarker(new MarkerOptions()
                            .position(new LatLng(lat, lon))
                            .title("Mi posición"));
            CameraUpdate center = CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 5);
            mMap.moveCamera(center);
        }
        //Toast.makeText(this, "Lat: " + lat + " lon: " + lon, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "destroy", Toast.LENGTH_LONG).show();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    this.ACCESS_FINE_LOCATION_REMOVE_CODE);
        } else {
            mLocationManager.removeUpdates(mLocationListener);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case ACCESS_FINE_LOCATION_PERMISSION_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(this, "Se requiere permiso para GPS.", Toast.LENGTH_LONG).show();
                    }
                    else {
                        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mLocationListener);
                        //mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, mLocationListener);
                    }
                }
                else{
                    Toast.makeText(this, "Se requiere permiso para GPS.", Toast.LENGTH_LONG).show();
                }
                break;
            case ACCESS_FINE_LOCATION_REMOVE_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(this, "Se requiere permiso para GPS.", Toast.LENGTH_LONG).show();
                    }
                    else {
                        mLocationManager.removeUpdates(mLocationListener);
                    }
                }
                else{
                    Toast.makeText(this, "Se requiere permiso para GPS.", Toast.LENGTH_LONG).show();
                }
                break;
            case GET_LAST_KNOWN_LOCATION_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(this, "Se requiere permiso para GPS.", Toast.LENGTH_LONG).show();
                    }
                    else {
                        Location lastKnownLocation = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                        if(lastKnownLocation != null){
                            mLocation = lastKnownLocation;
                            updateMarker(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude());
                        }
                    }
                }
                else{
                    Toast.makeText(this, "Se requiere permiso para GPS.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        if (destinationMarker != null)
            destinationMarker.remove();
        destinationMarker = mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .title("Destino"));
        destinationMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
    }

    private class GetGooglrDirectionsTask extends AsyncTask<Void, Void, Response>{
        private LatLng origin;
        private LatLng destination;
        private Context mContext;

        public GetGooglrDirectionsTask(Context context, LatLng origin, LatLng destination){
            mContext = context;
            this.origin = origin;
            this.destination = destination;
        }

        @Override
        protected Response doInBackground(Void... voids) {
            DirectionsApiController controller = new DirectionsApiController();
            return controller.getDirections(mContext, origin, destination);
        }

        @Override
        protected void onPostExecute(Response response) {
            super.onPostExecute(response);
            if(response != null){
                OverviewPolyline encodedPolyline = response.routes[0].overview_polyline;
                List<LatLng> polylinePoints = PolylineParser.decodePoly(encodedPolyline.points);
                if(mMap != null){
                    for(LatLng point : polylinePoints){
                        mMap.addPolyline(new PolylineOptions()
                                                .add(point));
                    }
                }
            }
            progressDialog.dismiss();
        }
    }
}
